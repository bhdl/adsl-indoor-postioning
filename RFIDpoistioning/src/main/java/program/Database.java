package program;

import java.util.ArrayList;
import java.util.List;
import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.MongoClient;
import objects.Position;
import objects.RFIDmeasurement;


public class Database {
	private final String rfidDatabaseName = "rfid_database";
	private final String rfidCollectionName = "rfid_collection";
	private final String positioningDatabaseName = "pos_database";
	private final String positioningCollectionName = "pos_collection";
	
	private DB rfidDatabase;
	private DB positionsDatabase;
	private DBCollection rfidCollection;
	private DBCollection positionsCollection;
	
	private final String URL;
	private final MongoClient mongoClient;
	
	private final Positioning POSITIONING;
	
	private final API api = new API();
	private final int sendMaxThisMutchRfidTagPosition = 100;
	
	
	public Database(String databaseUrl) {
		this.POSITIONING = new Positioning();
		this.URL = databaseUrl;
		//this.mongoClient = new MongoClient(URL);	//TODO
		this.mongoClient = new MongoClient();
		openRFIDdatabase();
		openPositionsDatabase();
	}
	
	
	public ArrayList<Position> startProcessing() {
		rfidCollection.drop();
		positionsCollection.drop();
		
		generateTestRFIDdetections();
		generateTestPositionDetections();
		
		ArrayList<Position> rfidTagPositions = new ArrayList<Position>();
		ArrayList<String> allRFIDtags = listAllDifferentRFIDtags();			//Works
		
		int positionsCounter = 0;
		for(String rfidTag : allRFIDtags) {
			ArrayList<RFIDmeasurement> rfidMeasurementsForTag = getAllMeasurementsForGivenRFIDtag(rfidTag);
			for(RFIDmeasurement rfidMeasurement : rfidMeasurementsForTag) {
				assignPositionToRFIDmeasurement(rfidMeasurement);
			}
			
			rfidTagPositions.add(POSITIONING.calculateRFIDTagPosition(rfidMeasurementsForTag));
			if(positionsCounter == sendMaxThisMutchRfidTagPosition) {
				boolean wasItSuccesfull = api.sendPositionsAPI(rfidTagPositions);
				if(wasItSuccesfull) rfidTagPositions.clear();
			}
			positionsCounter++;
		}
		
		while(rfidTagPositions.isEmpty()) {
			boolean wasItSuccesfull = api.sendPositionsAPI(rfidTagPositions);
			if(wasItSuccesfull) rfidTagPositions.clear();
		}
		
		return rfidTagPositions;
    }
	
	
	
	private void openRFIDdatabase() {
		rfidDatabase = mongoClient.getDB(rfidDatabaseName);
		rfidCollection = rfidDatabase.getCollection(rfidCollectionName);
	}
	
	private void openPositionsDatabase() {
		positionsDatabase = mongoClient.getDB(positioningDatabaseName);
		positionsCollection = rfidDatabase.getCollection(positioningCollectionName);
	}
	
	
	private ArrayList<String> listAllDifferentRFIDtags() {
		List<DBCollection> distinctRFIDtagIDs = rfidCollection.distinct("data.rf_data.tag_id");
		ArrayList<String> rfidTags = new ArrayList<String>(distinctRFIDtagIDs.size());
		for(int i=0; i < distinctRFIDtagIDs.size(); i++) {
			rfidTags.add(distinctRFIDtagIDs.get(i)+"");
			System.out.println("RFID tag: " + distinctRFIDtagIDs.get(i));
		}
		return rfidTags;
	}
	
	
	
	private ArrayList<RFIDmeasurement> getAllMeasurementsForGivenRFIDtag(String rfidTagID) {
		ArrayList<RFIDmeasurement> rfidMeasurements = new ArrayList<RFIDmeasurement>();
		
		BasicDBObject searchQuery = new BasicDBObject();
		searchQuery.put("data.rfData.tagId", rfidTagID);
		
		DBCursor cursor = rfidCollection.find(searchQuery);
		while(cursor.hasNext()) {
			BasicDBObject result = (BasicDBObject) cursor.next();
			BasicDBObject data = (BasicDBObject) result.get("data");
			BasicDBObject rfData = (BasicDBObject) data.get("rfData");
			BasicDBObject readerStatus = (BasicDBObject) data.get("readerStatus");
			
			RFIDmeasurement rfidMeasurement = new RFIDmeasurement();
			rfidMeasurement.setTagID(rfData.getString("tagId"));
			rfidMeasurement.setAntennaID(rfData.getString("antennaId"));
			rfidMeasurement.setRssi(rfData.getInt("rssi"));
			rfidMeasurement.setTimestamp(rfData.getLong("timestamp"));
			rfidMeasurement.setReaderTemperature(readerStatus.getInt("temperature"));
			
			rfidMeasurements.add(rfidMeasurement);
			
			//System.out.println("Tag id: " + rfidMeasurement.getTagID() + "  " + rfidMeasurement.getAntennaID()
			//	+ "  " + rfidMeasurement.getRssi() + "  " + rfidMeasurement.getTimestamp() + "  " + rfidMeasurement.getReaderTemperature());
		}
		return rfidMeasurements;
	}
	
	
	private void assignPositionToRFIDmeasurement(RFIDmeasurement rfidMeasurement) {
		Position dronePositionAtTheTime = getDronePositionAtTimestamp(rfidMeasurement.getTimestamp());
		rfidMeasurement.setDetectorPositionLat((float) dronePositionAtTheTime.getLat());
		rfidMeasurement.setDetectorPositionLon((float) dronePositionAtTheTime.getLon());
		rfidMeasurement.setDetectorPositionY((float) dronePositionAtTheTime.getY());
		rfidMeasurement.setDetectorPositionZ((float) dronePositionAtTheTime.getZ());
		rfidMeasurement.setDetectorPositionAccuracy((float) dronePositionAtTheTime.getAccuracy());
	}
	
	
	private Position getDronePositionAtTimestamp(long timestamp) {
		DBObject posAfter = positionsCollection.findOne(new BasicDBObject("data.position.timestamp", new BasicDBObject("$gte", timestamp)), 
				new BasicDBObject(), new BasicDBObject("data.position.timestamp", 1));
		
		BasicDBObject dataAfter = (BasicDBObject) posAfter.get("data");
		BasicDBObject positionsAfter = (BasicDBObject) dataAfter.get("position");
		BasicDBObject relativePositionAfter = (BasicDBObject) positionsAfter.get("relativeLocation");
		BasicDBObject geoLocationAfter = (BasicDBObject) positionsAfter.get("geoLocation");
		
		
		DBObject posBefore = positionsCollection.findOne(new BasicDBObject("data.position.timestamp", new BasicDBObject("$lte", timestamp)), 
				new BasicDBObject(), new BasicDBObject("data.position.timestamp", -1));
		
		BasicDBObject dataBefore = (BasicDBObject) posBefore.get("data");
		BasicDBObject positionsBefore = (BasicDBObject) dataBefore.get("position");
		BasicDBObject relativePositionBefore = (BasicDBObject) positionsBefore.get("relativeLocation");
		BasicDBObject geoLocationBefore = (BasicDBObject) positionsBefore.get("geoLocation");
		
		
		long timeAfter = positionsAfter.getLong("timestamp");
		double xAfter = relativePositionAfter.getDouble("x");
		double yAfter = relativePositionAfter.getDouble("y");
		double zAfter = relativePositionAfter.getDouble("z");
		double accuracyAfter = relativePositionAfter.getDouble("accuracy");
		double latAfter = geoLocationAfter.getDouble("latitude");
		double lonAfter = geoLocationAfter.getDouble("longitude");
		
		long timeBefore = positionsBefore.getLong("timestamp");
		double xBefore = relativePositionBefore.getDouble("x");
		double yBefore = relativePositionBefore.getDouble("y");
		double zBefore = relativePositionBefore.getDouble("z");
		double accuracyBefore = relativePositionBefore.getDouble("accuracy");
		double latBefore = geoLocationBefore.getDouble("latitude");
		double lonBefore = geoLocationBefore.getDouble("longitude");
		
		
		Position interpolatedPosition;
		if(timeAfter == timestamp) {
			interpolatedPosition = new Position(xAfter, yAfter, zAfter, accuracyAfter, latAfter, lonAfter);
		} else if(timeBefore == timestamp) {
			interpolatedPosition = new Position(xBefore, yBefore, zBefore, accuracyBefore, latBefore, lonBefore);
		} else {
			double beforeWeight = 1.0 / Math.abs(timeBefore - timestamp);
			double afterWeight = 1.0 / Math.abs(timeAfter - timestamp);
			double weightSumm = beforeWeight + afterWeight;
			interpolatedPosition = new Position(
					(beforeWeight * xBefore + afterWeight * xAfter) / weightSumm,
					(beforeWeight * yBefore + afterWeight * yAfter) / weightSumm,
					(beforeWeight * zBefore + afterWeight * zAfter) / weightSumm,
					(beforeWeight * latBefore + afterWeight * latAfter) / weightSumm,
					(beforeWeight * lonBefore + afterWeight * lonAfter) / weightSumm,
					(beforeWeight * accuracyBefore + afterWeight * accuracyAfter) / weightSumm
			);
		}
		
		System.out.println("Position: " + interpolatedPosition.toString());
		return interpolatedPosition;
	}
	
	
	/*private void exists() {
		BasicDBObject searchQuery = new BasicDBObject();
		searchQuery.put("name", new BasicDBObject("$exists", true));
		DBCursor cursor = rfidCollection.find(searchQuery);
	}*/
	
	
	//------------------------------------------------------------------------
	public void generateTestRFIDdetections() {
		for(int i=0; i < 10; i++) {
			BasicDBObject rfidMeasurement = new BasicDBObject();
			rfidMeasurement.put("timestamp", 100 + i);
			rfidMeasurement.put("clientId", "cl01");
			rfidMeasurement.put("type", "rfid");
			rfidMeasurement.put("package", "cl01_2018_08_21_10_43_21_123");
			
			BasicDBObject data = new BasicDBObject();
			
			BasicDBObject readerStatus = new BasicDBObject();
			readerStatus.put("temperature", 48);
			data.put("readerStatus", readerStatus);
			
			BasicDBObject rfdata = new BasicDBObject();
			rfdata.put("tagId", "rf0" + i);
			rfdata.put("antennaId", "antenna_id");
			rfdata.put("rssi", -85);
			rfdata.put("timestamp", 400-i*10);
				
			data.put("rfData", rfdata);
			rfidMeasurement.put("data", data);
			
			rfidCollection.insert(rfidMeasurement);
		}
		
		BasicDBObject rfidMeasurement = new BasicDBObject();
		rfidMeasurement.put("timestamp", 1005);
		rfidMeasurement.put("antennaId", "cl01");
		rfidMeasurement.put("type", "rfid");
		rfidMeasurement.put("package", "cl01_2018_08_21_10_43_21_123");
		
		BasicDBObject data = new BasicDBObject();
		
		BasicDBObject readerStatus = new BasicDBObject();
		readerStatus.put("temperature", 48);
		data.put("readerStatus", readerStatus);
		
		BasicDBObject rfdata = new BasicDBObject();
		rfdata.put("tagId", "rf0" + 5);
		rfdata.put("antennaId", "antenna_id");
		rfdata.put("rssi", -85);
		rfdata.put("timestamp", 135);
		
		data.put("rfData", rfdata);
		rfidMeasurement.put("data", data);
		
		rfidCollection.insert(rfidMeasurement);
	}
	
	
	public void generateTestPositionDetections() {
		for(int i=0; i < 10; i++) {
			BasicDBObject measurement = new BasicDBObject();
			measurement.put("timestamp", 100 + i);
			measurement.put("clientId", "cl01");
			measurement.put("type", "position");
			measurement.put("package", "cl01_2018_08_21_10_43_21_123");
			
			BasicDBObject data = new BasicDBObject();
			
			BasicDBObject position = new BasicDBObject();
			position.put("tagId", 1);
			position.put("timestamp", i*100);
			
			BasicDBObject relativeLocation = new BasicDBObject();
			relativeLocation.put("x", 75 + 5*i);
			relativeLocation.put("y", 150 + 10*i);
			relativeLocation.put("z", 7.5);
			relativeLocation.put("accuracy", 0.75);
			position.put("relativeLocation", relativeLocation);
			
			BasicDBObject geoLocation = new BasicDBObject();
			geoLocation.put("latitude", 1.123);
			geoLocation.put("longitude", 1.123);
			position.put("geoLocation", geoLocation);
			
			data.put("position", position);
			measurement.put("data", data);
			
			positionsCollection.insert(measurement);
			System.out.println(measurement);
		}
	}
}
