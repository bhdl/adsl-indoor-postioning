package program;

import java.util.ArrayList;
import objects.Position;
import objects.RFIDmeasurement;

//-45 db -> weight 4,938e-4
//-60 db -> weight 2,777e-4
//-75 db -> weight 1,777e-4
//RFID range: 3-5 m   60-90 db
//Avg positioning accuracy: 0.25 - 0.5 m 


public class Positioning {
	public Position calculateRFIDTagPosition(ArrayList<RFIDmeasurement> measurementsForGivenRFIDtag) {
		float avgX = 0;
		float avgY = 0;
		float avgZ = 0;
		float avgLat = 0;
		float avgLon = 0;
		float weightSum = 0;
		
		float detectorPositionAccuracy = 0;
		
		for(RFIDmeasurement measurement : measurementsForGivenRFIDtag) {
			//Add the drone position accuracy as extra rssi to the measurement.
			float droneAccuracyToRssi = (float) Math.pow(measurement.getDetectorPositionAccuracy(), 2);
			if(measurement.getDetectorPositionAccuracy() < 1) droneAccuracyToRssi = measurement.getDetectorPositionAccuracy() * 10;
			
			float modifiedRssi = Math.abs(measurement.getRssi()) + Math.abs(droneAccuracyToRssi);
			
			float weight = 1;
			if(modifiedRssi > 1 || modifiedRssi < -1) weight = 1f / (modifiedRssi*modifiedRssi);
			
			detectorPositionAccuracy += measurement.getDetectorPositionAccuracy();
			
			weightSum += weight;
			avgX += weight * measurement.getDetectorPositionX();
			avgY += weight * measurement.getDetectorPositionY();
			avgZ += weight * measurement.getDetectorPositionZ();
			avgLat += weight * measurement.getDetectorPositionLat();
			avgLon += weight * measurement.getDetectorPositionLon();
		}
		
		detectorPositionAccuracy /= measurementsForGivenRFIDtag.size();
		
		
		Position position = new Position(
				avgX / weightSum,
				avgY / weightSum,
				avgZ / weightSum,
				detectorPositionAccuracy,
				avgLat / weightSum,
				avgLon / weightSum,
				measurementsForGivenRFIDtag.get(0).getTagID()
		);
		return position;
	}
}
