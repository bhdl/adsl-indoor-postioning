package program;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Iterator;

import org.json.JSONArray;
import org.json.JSONObject;
import objects.Position;


public final class API {
	private final String GET = "GET";
	private final String POST = "POST";
	
	private final String URL = "";			//TODO
	private final int TIMEOUT = 5000;
	
	
	public boolean sendPositionsAPI(ArrayList<Position> positions) {
		JSONObject sendData = positionsToJSON(positions);
		JSONObject response = apiCall(URL, POST, "", false);
		return isResponseValid(response);
	}
	
	
	private JSONObject positionsToJSON(ArrayList<Position> positions) {
		JSONObject send = new JSONObject();
		try {
			send.put("type", "rfidPositions");
			send.put("timestamp", System.currentTimeMillis());
			
			JSONArray positionsArray = new JSONArray();
			for(Position position : positions) {
				JSONObject pos = new JSONObject();
				pos.put("ID", position.getRfidTagID());
				pos.put("x", position.getX());
				pos.put("y", position.getY());
				pos.put("z", position.getZ());
				pos.put("latitude", position.getLat());
				pos.put("longitude", position.getLon());
				
				positionsArray.put(pos);
			}
			send.put("positions", positionsArray);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return send;
	}
	
	
	private boolean isResponseValid(JSONObject response) {
		try {
			return response.get("status").equals("OK");
		} catch(Exception e) {
			return false;
		}
	}
	
	
	
	private JSONObject apiCall(String urlAddress, String requestMethod, String message, boolean doOutput) {
		System.out.println("API send: " + message + "  " + urlAddress);
		try {
            URL url = new URL(urlAddress);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setReadTimeout(TIMEOUT);
            conn.setConnectTimeout(TIMEOUT);
            conn.setRequestMethod(requestMethod);
            conn.setDoInput(true);
            conn.setDoOutput(doOutput);
            if(doOutput) {
            	conn.setRequestProperty("Content-Type", "application/json");
                conn.getOutputStream().write(message.getBytes("UTF-8"));
            }
            
            BufferedReader br = null;
            if (conn.getResponseCode() == HttpURLConnection.HTTP_OK) {
                br = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            } else {
                br = new BufferedReader(new InputStreamReader(conn.getErrorStream()));
                System.out.println("API error: " + conn.getResponseCode());
            }
            
            String line;
            String read = "";
            if (br != null) {
            	while ((line = br.readLine()) != null) {
                	read += line;
                }
            }
            conn.disconnect();
            System.out.println("API response: " + read);
            return new JSONObject(read);
		} catch (Exception e) {
			System.err.println("Error in api call!");
			e.printStackTrace();
			return null;
		}
	}
}
