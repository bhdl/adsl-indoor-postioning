package objects;

public class Position {
	private String rfidTagID;
	private double lat;
	private double lon;
	
	private double x;
	private double y;
	private double z;
	
	private double accuracy;
	private long timestamp;
	
	
	public Position() {}
	
	public Position(double x, double y, double z) {
		this.x = x;
		this.y = y;
		this.z = z;
	}
	
	public Position(double x, double y, double z, double accuracy) {
		this.x = x;
		this.y = y;
		this.z = z;
		this.accuracy = accuracy;
	}
	
	public Position(double x, double y, double z, double accuracy, double lat, double lon) {
		this.x = x;
		this.y = y;
		this.z = z;
		this.accuracy = accuracy;
		this.lat = lat;
		this.lon = lon;
	}
	
	public Position(double x, double y, double z, double accuracy, double lat, double lon, String rfidTagId) {
		this.x = x;
		this.y = y;
		this.z = z;
		this.accuracy = accuracy;
		this.lat = lat;
		this.lon = lon;
		this.rfidTagID = rfidTagId;
	}
	
	
	public String toString() {
		return "X: " + x + "  Y: " + y + "  Z: " + z + "  Acc: " + accuracy + "  Lat: " + lat + "  Lon: " + lon + "  Timestamp: " + timestamp;
	}
	
	
	public double getLat() {
		return lat;
	}
	public void setLat(double lat) {
		this.lat = lat;
	}

	public double getLon() {
		return lon;
	}
	public void setLon(double lon) {
		this.lon = lon;
	}
	
	public double getX() {
		return x;
	}
	public void setX(double x) {
		this.x = x;
	}

	public double getY() {
		return y;
	}
	public void setY(double y) {
		this.y = y;
	}

	public double getZ() {
		return z;
	}
	public void setZ(double z) {
		this.z = z;
	}

	public double getAccuracy() {
		return accuracy;
	}
	public void setAccuracy(double value) {
		this.accuracy = value;
	}
	
	public long getTimestamp() {
		return timestamp;
	}
	public void setTimestamp(long timestamp) {
		this.timestamp = timestamp;
	}

	public String getRfidTagID() {
		return rfidTagID;
	}

	public void setRfidTagID(String rfidTagID) {
		this.rfidTagID = rfidTagID;
	}
}