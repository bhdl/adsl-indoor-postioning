package objects;

public class RFIDmeasurement {
	//RFID position
	private String tagID;
	private String antennaID;
	private int rssi;
	private long timestamp;
	private int readerTemperature;
	
	//Drone position
	private String droneID;
	private float detectorPositionLat;
	private float detectorPositionLon;
	private float detectorPositionX;
	private float detectorPositionY;
	private float detectorPositionZ;
	private float detectorPositionAccuracy;
	//private float detectorAngleToWarehouse;
	
	
	public float getDetectorPositionLat() {
		return detectorPositionLat;
	}
	public void setDetectorPositionLat(float detectorPositionLat) {
		this.detectorPositionLat = detectorPositionLat;
	}
	
	public float getDetectorPositionLon() {
		return detectorPositionLon;
	}
	public void setDetectorPositionLon(float detectorPositionLon) {
		this.detectorPositionLon = detectorPositionLon;
	}
	
	public String getTagID() {
		return tagID;
	}
	public void setTagID(String tagID) {
		this.tagID = tagID;
	}
	
	public String getAntennaID() {
		return antennaID;
	}
	public void setAntennaID(String antennaID) {
		this.antennaID = antennaID;
	}
	
	public int getRssi() {
		return rssi;
	}
	public void setRssi(int rssi) {
		this.rssi = rssi;
	}
	
	public long getTimestamp() {
		return timestamp;
	}
	public void setTimestamp(long timestamp) {
		this.timestamp = timestamp;
	}
	
	public int getReaderTemperature() {
		return readerTemperature;
	}
	public void setReaderTemperature(int readerTemperature) {
		this.readerTemperature = readerTemperature;
	}
	
	public String getDroneID() {
		return droneID;
	}
	public void setDroneID(String droneID) {
		this.droneID = droneID;
	}
	
	public float getDetectorPositionX() {
		return detectorPositionX;
	}
	public void setDetectorPositionX(float detectorPositionX) {
		this.detectorPositionX = detectorPositionX;
	}
	
	public float getDetectorPositionY() {
		return detectorPositionY;
	}
	public void setDetectorPositionY(float detectorPositionY) {
		this.detectorPositionY = detectorPositionY;
	}
	
	public float getDetectorPositionZ() {
		return detectorPositionZ;
	}
	public void setDetectorPositionZ(float detectorPositionZ) {
		this.detectorPositionZ = detectorPositionZ;
	}
	
	public float getDetectorPositionAccuracy() {
		return detectorPositionAccuracy;
	}
	public void setDetectorPositionAccuracy(float detectorPositionAccuracy) {
		this.detectorPositionAccuracy = detectorPositionAccuracy;
	}
	
	/*public float getDetectorAngleToWarehouse() {
		return detectorAngleToWarehouse;
	}
	public void setDetectorAngleToWarehouse(float detectorAngleToWarehouse) {
		this.detectorAngleToWarehouse = detectorAngleToWarehouse;
	}*/
}
