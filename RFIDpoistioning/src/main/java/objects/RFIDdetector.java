package objects;

public class RFIDdetector {
	private String droneID;
	private String detectorID;
	private float perpendicularAntennaAngleRelativeToDrone;		//The perpendicular angle of the detector.
	private float antennaAngleRange;							//180 degree in total.
	
	
	public String getDroneID() {
		return droneID;
	}
	public void setDroneID(String droneID) {
		this.droneID = droneID;
	}
	
	public String getDetectorID() {
		return detectorID;
	}
	public void setDetectorID(String detectorID) {
		this.detectorID = detectorID;
	}
	
	public float getPerpendicularAntennaAngleRelativeToDrone() {
		return perpendicularAntennaAngleRelativeToDrone;
	}
	public void setPerpendicularAntennaAngleRelativeToDrone(float perpendicularAntennaAngleRelativeToDrone) {
		this.perpendicularAntennaAngleRelativeToDrone = perpendicularAntennaAngleRelativeToDrone;
	}
	
	public float getAntennaAngleRange() {
		return antennaAngleRange;
	}
	public void setAntennaAngleRange(float antennaAngleRange) {
		this.antennaAngleRange = antennaAngleRange;
	}
}
