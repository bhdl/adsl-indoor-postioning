package arteries;

import org.junit.Test;
import data.GPScoordinate;
import junit.framework.TestCase;


public class CalculationsTests extends TestCase {
	@Test
 	public void testGPSdistanceAddition() {
		GPScoordinate gps = new GPScoordinate(0, 0, 0);
		assertEquals(0f, gps.getBearingAngleOfRelativeCoordinates(0, 100));
		assertEquals(45f, gps.getBearingAngleOfRelativeCoordinates(100, 100));
		assertEquals(90f, gps.getBearingAngleOfRelativeCoordinates(100, 0));
		assertEquals(360-45f, gps.getBearingAngleOfRelativeCoordinates(-100, 100));
		assertEquals(270f, gps.getBearingAngleOfRelativeCoordinates(-100, 0));
    }
	
	@Test
 	public void testBearingCalculation() {
		GPScoordinate gps = new GPScoordinate(0, 0, 0);
		assertEquals(0f, gps.getBearingAngleOfRelativeCoordinates(0, 100));
		assertEquals(45f, gps.getBearingAngleOfRelativeCoordinates(100, 100));
		assertEquals(90f, gps.getBearingAngleOfRelativeCoordinates(100, 0));
		assertEquals(360-45f, gps.getBearingAngleOfRelativeCoordinates(-100, 100));
		assertEquals(270f, gps.getBearingAngleOfRelativeCoordinates(-100, 0));
    }
	
	@Test
 	public void testdistanceAddition() {
		GPScoordinate original = new GPScoordinate(42, 16, 0);
		for(int distance = 1; distance < 1000; distance += 25) {
			for(int i=0; i < 360; i++) {
				GPScoordinate added = original.addDistanceToCoordinate(distance, i);
				assertEquals(distance, Math.round(gpsCoordDistance(original, added)));
			}
		}
	}
	

	private double gpsCoordDistance(GPScoordinate original, GPScoordinate added) {
	    final int R = 6371; 				//Radius of the earth
	    double latDistance = Math.toRadians(original.getLatitude() - added.getLatitude());
	    double lonDistance = Math.toRadians(original.getLongitude() - added.getLongitude());
	    double a = Math.sin(latDistance / 2) * Math.sin(latDistance / 2) + Math.cos(Math.toRadians(original.getLatitude())) * 
	    		Math.cos(Math.toRadians(added.getLatitude())) * Math.sin(lonDistance / 2) * Math.sin(lonDistance / 2);
	    double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
	    double distance = R * c * 1000; 	//Convert to meters
	    double height = original.getAltitude() - added.getAltitude();

	    distance = Math.pow(distance, 2) + Math.pow(height, 2);
	    return Math.sqrt(distance);
	}
}
