package buffer;

import java.util.ArrayList;
import data.ReceivedSignal;


public class MeasuringDetector {
	private String detectorID;
	private final ArrayList<ReceivedSignal> RECEIVED_SIGNALS = new ArrayList<ReceivedSignal>();
	
	
	public MeasuringDetector(String detectorID) {
		this.detectorID = detectorID;
	}
	

	public String getDetectorID() {
		return detectorID;
	}
	public void setDetectorID(String detectorID) {
		this.detectorID = detectorID;
	}
	
	public ArrayList<ReceivedSignal> getReceivedSignals() {
		return RECEIVED_SIGNALS;
	}
	
	
	public void addReceivedSignal(ReceivedSignal receviedSignal) {
		RECEIVED_SIGNALS.add(receviedSignal);
	}
	
	public ReceivedSignal getAveragedReceivedSignal() {
		if(RECEIVED_SIGNALS.isEmpty()) return null;
		ReceivedSignal averaged = RECEIVED_SIGNALS.get(RECEIVED_SIGNALS.size()-1).copy();
		
		float avgDistance = 0;
		float avgAccuracy = 0;
		for(ReceivedSignal receivedSignal : RECEIVED_SIGNALS) {
			avgDistance += receivedSignal.getDistance();
			avgAccuracy += receivedSignal.getAccuracy();
		}
		averaged.setDistance(avgDistance / (float)RECEIVED_SIGNALS.size());
		averaged.setAccuracy(avgAccuracy / (float)RECEIVED_SIGNALS.size());
		return averaged;
	}
}
