package buffer;

import java.util.ArrayList;
import data.ReceivedSignal;


public class BeaconDetection {
	private String ID;
	private final ArrayList<MeasuringDetector> beaconMeasuringDetectors = new ArrayList<MeasuringDetector>();
	
	
	public BeaconDetection(String ID) {
		this.ID = ID;
	}
	
	public String getID() {
		return ID;
	}
	public void setID(String iD) {
		ID = iD;
	}
	
	public ArrayList<MeasuringDetector> getBeaconMeasuringDetectors() {
		return this.beaconMeasuringDetectors;
	}
	
	
	public void searchAndAddSignalToMeasurementsList(ReceivedSignal receviedSignal) {
		boolean found = false;
		for(MeasuringDetector detector : beaconMeasuringDetectors) {
			if(detector.getDetectorID().equals(receviedSignal.getDetectorID())) {
				found = true;
				detector.addReceivedSignal(receviedSignal);
				break;
			}
		}
		
		if(!found) {
			MeasuringDetector beaconMeasuringDetector = new MeasuringDetector(receviedSignal.getDetectorID());
			beaconMeasuringDetector.addReceivedSignal(receviedSignal);
			beaconMeasuringDetectors.add(beaconMeasuringDetector);
		}
	}
}
