package buffer;

import java.util.ArrayList;
import data.DetectedBeacon;
import data.ReceivedSignal;
import program.Values;


public class Buffer {
	private final ArrayList<BeaconDetection> BEACONS_BUFFER = new ArrayList<BeaconDetection>();
	
	
	public void addMeasurement(ArrayList<DetectedBeacon> newReceviedSignals) {
		for(DetectedBeacon detectedBeacon : newReceviedSignals) {
			if(detectedBeacon.getReceivingDetectors().isEmpty()) continue; 
			
			boolean found = false;
			for(BeaconDetection bufferedBeacon : BEACONS_BUFFER) {					//Search the buffer for a detected beacon with the same ID
				if(bufferedBeacon.getID().equals(detectedBeacon.getID())) {			//Check if the beacons have the same ID
					//Find and store every new signal ID in the buffered beacon's signals list.
					for(ReceivedSignal signal : detectedBeacon.getReceivingDetectors()) bufferedBeacon.searchAndAddSignalToMeasurementsList(signal);
					found = true;
					break;
				}
			}
			
			if(!found) {
				BeaconDetection beacon = new BeaconDetection(detectedBeacon.getID());
				for(ReceivedSignal signal : detectedBeacon.getReceivingDetectors())  beacon.searchAndAddSignalToMeasurementsList(signal);
				BEACONS_BUFFER.add(beacon);
			}
		}
	}
	
	
	
	public ArrayList<DetectedBeacon> calcAveragedBeaconMeasurements() {
		clearOldMeasurements();
		
		ArrayList<DetectedBeacon> detectedBeacons = new ArrayList<DetectedBeacon>();
		
		for(BeaconDetection beaconDetection : BEACONS_BUFFER) {
			DetectedBeacon detectedBeacon = new DetectedBeacon();
			detectedBeacon.setID(beaconDetection.getID());
			
			for(MeasuringDetector beaconMeasuringDetector : beaconDetection.getBeaconMeasuringDetectors()) {
				ReceivedSignal averagedSignal = beaconMeasuringDetector.getAveragedReceivedSignal();
				if(averagedSignal == null) continue;
				
				if(detectedBeacon.getDetectionTimestamp() < averagedSignal.getTimestamp()) detectedBeacon.setDetectionTimestamp(averagedSignal.getTimestamp());
				detectedBeacon.getReceivingDetectors().add(averagedSignal);
			}
			
			if(!detectedBeacon.getReceivingDetectors().isEmpty()) detectedBeacons.add(detectedBeacon);
		}
		return detectedBeacons;
	}
	
	
	public void clearBuffer() {
		BEACONS_BUFFER.clear();
	}
	
	
	private void clearOldMeasurements() {
		long currentTime = System.currentTimeMillis();
		for(int i = BEACONS_BUFFER.size()-1; i > -1; i--) {
			
			ArrayList<MeasuringDetector> beaconMeasuringDetectors = BEACONS_BUFFER.get(i).getBeaconMeasuringDetectors();
			for(int j = beaconMeasuringDetectors.size()-1; j > -1; j--) {
				
				ArrayList<ReceivedSignal> receivedSignals = beaconMeasuringDetectors.get(j).getReceivedSignals();
				for(int z = receivedSignals.size()-1; z > -1; z--) {
					if(currentTime - receivedSignals.get(z).getTimestamp() > Values.getSignalsBufferIntervall()) receivedSignals.remove(z);
				}
				if(receivedSignals.isEmpty()) beaconMeasuringDetectors.remove(j);
			}
			if(beaconMeasuringDetectors.isEmpty()) BEACONS_BUFFER.remove(i);
		}
	}
	
	
	public void printBuffer() {
		for(BeaconDetection beaconDetection : BEACONS_BUFFER) {
			//System.out.println("Beacon ID: " + beaconDetection.getID());
			ArrayList<MeasuringDetector> beaconMeasuringDetectors = beaconDetection.getBeaconMeasuringDetectors();
			for(MeasuringDetector beaconMeasuringDetector : beaconMeasuringDetectors) {
				System.out.print("Bc: " + beaconDetection.getID() + "  Det ID: " + beaconMeasuringDetector.getDetectorID() + "  ");
				ArrayList<ReceivedSignal> receivedSignals = beaconMeasuringDetector.getReceivedSignals();
				for(ReceivedSignal receivedSignal : receivedSignals) {
					System.out.print(receivedSignal.getDistance() + " ");
				}
				System.out.println();
			}
		}
	}
}
