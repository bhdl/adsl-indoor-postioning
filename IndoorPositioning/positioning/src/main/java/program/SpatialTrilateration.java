package program;

import java.util.ArrayList;
import java.util.Random;
import data.Bounds;
import data.Detector;
import data.Position;
import data.ReceivedSignal;
import data.ReceivingDetector;

/*
http://192.168.0.202:2000/tags			tagek legut�bbi adatai
http://192.168.0.202:2000/changes/		a v�ltoz�sok

3 api h�v�s lesz:
1) tags: Minden jelenlegi tag �llapota. (Legutols� �llapot, t�vols�g van ebben is). Ezt kell javar�szt h�vogatni. Nincs benne timestamp, ha nem sz�l egy jelad�, att�l m�g benne marad!
2) tags/ID Pl: tags/16
Ez csak a t�vols�got adja vissza (m) cm pontossan az adott jelad�ra.
3) changes
legutols� api h�v�s �ta v�ltoztak
Ha valami nincs, akkor azt nem l�tja.
Lehet lesz majd m�r�s index id (m�r�s azonos�t�).

Lesz egy socket is, amin folyamatosan kapom a leg�jabb adatokat. Minden nyers adat benne lesz.
Melyik fix pontt�l melyik tag, mely t�vra van.

Config filet le kell majd t�lteni. K�l�n API h�v�s lesz r�.
50 tag alatt lesz val�sz�n�leg a v�gs� rendszer.
A TAG-ek kioszt�sa dhcp szer�en megy. Kap egy tag-id,t amikor felcsatlakoznak.
Kehet, hogy 2 nek ugyan az lesz a tagja �ramsz�net eset�n. Ki kell venni az aksit �s visszatenni, ha ez van.

Ha kicsit a t�v, akkor t�bbet mutat. Ha ngyobb a t�vols�g, akkor jobb!

JSON �s x,y,z,id (string) a config form�tuma.

A poz�ci� visszak�ld�se:
Socketen kereszt�l megy vissza a poz�ci�:
{
	x: double
	y: double
	z: double
	id: int
	[ID amik a legjobbak. 6 kell, mert annyit fog csak haszn�lni a poz�cion�l�s.]
}

P�lda API h�v�s:
data	
16				//A jelad� ID ja
1	3.42		//A beacon ID-ja �s a t�vols�g t�le
2	3.81
3	3.71
4	2.6
5	2.67
*/

public class SpatialTrilateration {
	private ArrayList<Detector> detectors;
	
	private final double descentRate = 0.01;									//0.01
	private final double resolution = 0.00001;									//0.00001  Meter
	private final double momentumAlpha = 0.25;									//0.25
	private final int maxSgdIterations = 1000000;								//1000000
	private final double maxSgdGradientStepSize = 10;							//10
	private final float positionsOutsideDetectionRangePenaltyMultiplier = 25;	//25
	private final Random rand = new Random(123);
	
	
	public SpatialTrilateration(ArrayList<Detector> detectors) {
		this.detectors = detectors;
	}
	
	
	public Position calculatePosition(ArrayList<ReceivedSignal> receivedSignals) {
		return positioning(receivedSignals, false, -1, true);
	}
	public Position calculatePosition(ArrayList<ReceivedSignal> receivedSignals, boolean refinePosition) {
		return positioning(receivedSignals, false, -1, refinePosition);
	}
	
	public Position calculatePosition(ArrayList<ReceivedSignal> receivedSignals, double fixedZcoord) {
		return positioning(receivedSignals, true, fixedZcoord, true);
	}
	public Position calculatePosition(ArrayList<ReceivedSignal> receivedSignals, double fixedZcoord, boolean refinePosition) {
		return positioning(receivedSignals, true, fixedZcoord, refinePosition);
	}
	
	
	public ArrayList<ReceivingDetector> assignReceivedDetectors(ArrayList<ReceivedSignal> receivedSignals) {
		ArrayList<ReceivingDetector> receivingDetectors = new ArrayList<ReceivingDetector>();
		
		for(ReceivedSignal signal : receivedSignals) {
			int foundDetectorID = findIndexOfDetectorID(signal.getDetectorID());
			if(foundDetectorID < 0) continue;
			
			Detector correspondingDetector = detectors.get(foundDetectorID);
			
			ReceivingDetector receiver = new ReceivingDetector();
			receiver.setDetectorID( correspondingDetector.getID() );
			receiver.setDetectorPosX( correspondingDetector.getX() );
			receiver.setDetectorPosY( correspondingDetector.getY() );
			receiver.setDetectorPosZ( correspondingDetector.getZ() );			
			receiver.setDistance( signal.getDistance() );
			receiver.setAccuracy( signal.getAccuracy() );
			
			receivingDetectors.add(receiver);
		}
		return receivingDetectors;
	}
	
	
	
	private Position positioning(ArrayList<ReceivedSignal> receivedSignals, boolean useFixedZcoordinate, double fixedZcoord, boolean refinePosition) {
		//Assign detector coordinates to the measurements.
		ArrayList<ReceivingDetector> receivingDetectors = assignReceivedDetectors(receivedSignals);
		if(receivingDetectors.size() < 4) {
			System.err.println("ERROR: Not enough detection for 3D position calculation!");
			return null;
		}
		
		//Calculate the position. Converge from all detector.
		Position sgdPosition = null;
		for(ReceivingDetector receivingDetector : receivingDetectors) {
			Position calcPosition = positionCalculationWithSGD(receivingDetectors, new Position(receivingDetector.getDetectorPosX(), receivingDetector.getDetectorPosY(), 
										(useFixedZcoordinate ? fixedZcoord : receivingDetector.getDetectorPosZ())), useFixedZcoordinate);
			if(refinePosition) refinePosition(calcPosition, receivingDetectors, useFixedZcoordinate, fixedZcoord);
			if(sgdPosition == null || sgdPosition.getAccuracy() > calcPosition.getAccuracy()) sgdPosition = calcPosition;
		}
		
		sgdPosition.setBeaconID(receivedSignals.get(0).getBeaconID());
		sgdPosition.setTimestamp(receivedSignals.get(0).getTimestamp());
		sgdPosition.setAccuracy( calculateAccuracy(sgdPosition, receivingDetectors) );
		//sgdPosition.setAccuracy(3 * Math.sqrt(sgdPosition.getAccuracy()));
		return sgdPosition;
	}
	
	
	private void refinePosition(Position sgdPosition, ArrayList<ReceivingDetector> receivingDetectors, boolean useFixedZcoordinate, double fixedZcoord) {
		if(receivingDetectors.size() < 4) return;
		
		//Find the smallest local minima in the z axis. Start from booth above and belowe the initial position.
		if(!useFixedZcoordinate) {
			Bounds detectorBounds = calculateBoundrarys(receivingDetectors);
			Position topSgdPosition = positionCalculationWithSGD(receivingDetectors, new Position(sgdPosition.getX(), sgdPosition.getY(), sgdPosition.getZ() + detectorBounds.diameter), useFixedZcoordinate);
			Position btmSgdPosition = positionCalculationWithSGD(receivingDetectors, new Position(sgdPosition.getX(), sgdPosition.getY(), sgdPosition.getZ() - detectorBounds.diameter), useFixedZcoordinate);
			
			if(topSgdPosition.getAccuracy() > btmSgdPosition.getAccuracy()) sgdPosition = btmSgdPosition;
				else sgdPosition = topSgdPosition;
		}
		
		//Recalculate the position starting from the current, and see how much the position changes with a given detection skipped. Calculate an accuracy for them.
		double avgX = 0, avgY = 0, avgZ = 0, avgAccuracy = 0, weights = 0;
		for(int i=0; i < receivingDetectors.size(); i++) {
			ArrayList<ReceivingDetector> partialReceivingDetectors = (ArrayList<ReceivingDetector>) receivingDetectors.clone();
			partialReceivingDetectors.remove(i);
			
			Position positionWithSkippedBeacon = positionCalculationWithSGD(partialReceivingDetectors, new Position(sgdPosition.getX(), sgdPosition.getY(), sgdPosition.getZ()), useFixedZcoordinate);
			
			final double weight = 1.0 / positionWithSkippedBeacon.getAccuracy();
			weights += weight;
			avgX += positionWithSkippedBeacon.getX() * weight;
			avgY += positionWithSkippedBeacon.getY() * weight;
			avgZ += positionWithSkippedBeacon.getZ() * weight;
			avgAccuracy += positionWithSkippedBeacon.getAccuracy() * weight;
		}
		sgdPosition.setX(avgX / weights);
		sgdPosition.setY(avgY / weights);
		sgdPosition.setZ(avgZ / weights);
		sgdPosition.setAccuracy(avgAccuracy / weights);
		
		//sgdPosition = ransacRefinedPosition(sgdPosition, receivingDetectors, useFixedZcoordinate, fixedZcoord);
	}
	
	
	private double calculateAccuracy(final Position position, final ArrayList<ReceivingDetector> receivingDetectors) {
		double accuracy = 0;
		for(ReceivingDetector detector : receivingDetectors) {
			accuracy += (1.0/detector.getAccuracy()) * Math.abs(detector.getDistance() - calculateDistance(position.getX(), position.getY(), position.getZ(),  
						detector.getDetectorPosX(), detector.getDetectorPosY(), detector.getDetectorPosZ()));
		}
		accuracy /= receivingDetectors.size();
		return 3 * accuracy;
	}
	
	
	
	//----------------------------------------------------Position calculation methods------------------------------------------------------------------
	//summ: http://www.wolframalpha.com/input/?i=d+(a*10+*+((sqrt((x-c)%5E2+%2B+(y-v)%5E2+%2B+(z-b)%5E2)+-+d))%5E2)+%2F+dx
	//And the derivate.
	public double calculateTotalDistanceDiffBetweenPositionAndMeasuredDistances(double x, double y, double z, ArrayList<ReceivingDetector> receivingDetectors) {
		double summedTotalDistanceDifference = 0;
		for(ReceivingDetector detector : receivingDetectors) {
			final double currentDistanceFromDetector = calculateDistance(x, y, z,  detector.getDetectorPosX(), detector.getDetectorPosY(), detector.getDetectorPosZ());
			double distanceDiff = currentDistanceFromDetector - detector.getDistance();
			distanceDiff *= distanceDiff * detector.getAccuracy();
			
			//Noise can only be additive. The signal cant be any further than the detection range.
			//Multiply with a cosntant if the current position is outside the detected circle.
			if(currentDistanceFromDetector - detector.getDistance() > 0) distanceDiff *= positionsOutsideDetectionRangePenaltyMultiplier;
			
			summedTotalDistanceDifference += distanceDiff;
		}
		return summedTotalDistanceDifference;
	}
	
	
	private Position positionCalculationWithSGD(ArrayList<ReceivingDetector> receivingDetectors, Position bestInitialPosition, boolean fixedZcoordinate) {
		final double[] previousGradientsBuffer = new double[3];
		double currentPosX = bestInitialPosition.getX();
		double currentPosY = bestInitialPosition.getY();
		double currentPosZ = bestInitialPosition.getZ();
		
		for(int i = 0; i < maxSgdIterations; i++) {
			final double[] derivates = calculateProbabilityFunctionDerivateAtGivenPosition(currentPosX, currentPosY, currentPosZ,  receivingDetectors,  previousGradientsBuffer);
			currentPosX -= derivates[0];
			currentPosY -= derivates[1];
			if(!fixedZcoordinate) currentPosZ -= derivates[2];
			
			//If the gradient is smaller than the resolution, stop the convergence.
			if(Math.abs(derivates[0]) < resolution  &&  Math.abs(derivates[1]) < resolution  &&  Math.abs(derivates[2]) < resolution) break;
		}
		
		Position position = new Position(currentPosX, currentPosY, currentPosZ);
		position.setAccuracy( calculateTotalDistanceDiffBetweenPositionAndMeasuredDistances(currentPosX, currentPosY, currentPosZ,  receivingDetectors) );
		//System.out.println("N: " + i + "   " + position.toString());
		return position;
	}
	
	
	private double[] calculateProbabilityFunctionDerivateAtGivenPosition(double x, double y, double z, final ArrayList<ReceivingDetector> receivingDetectors, final double[] previousGradientsBuffer) {
		double dx = 0, dy = 0, dz = 0;
		for(ReceivingDetector detector : receivingDetectors) {
			final double detX = detector.getDetectorPosX();
			final double detY = detector.getDetectorPosY();
			final double detZ = detector.getDetectorPosZ();
			final double detAccuracy = detector.getAccuracy();
			final double r = detector.getDistance();
			
			if(detX == x  &&  detY == y  &&  detZ == z) {
				x += rand.nextInt(1000) / 100000000.0;
				y += rand.nextInt(1000) / 100000000.0;
				z += rand.nextInt(1000) / 100000000.0;
			} else {
				final double distanceFromDetector = Math.sqrt((detX - x)*(detX - x) + (detY - y)*(detY - y) + (detZ - z)*(detZ - z));
				double multiplier = 2.0 * (r - distanceFromDetector) / distanceFromDetector;
				
				//Noise can only be additive. The signal cant be any further than the detection range.
				//Multiply with a cosntant if the current position is outside the detected circle.
				if(distanceFromDetector - r > 0) multiplier *= positionsOutsideDetectionRangePenaltyMultiplier;
				
				dx += (detX - x) * multiplier * detAccuracy;
				dy += (detY - y) * multiplier * detAccuracy;
				dz += (detZ - z) * multiplier * detAccuracy;
			}
		}
		
		dx *= descentRate;
		dy *= descentRate;
		dz *= descentRate;
		
		
		//----Momentum
		dx += momentumAlpha * previousGradientsBuffer[0];
		dy += momentumAlpha * previousGradientsBuffer[1];
		dz += momentumAlpha * previousGradientsBuffer[2];
		
		previousGradientsBuffer[0] = dx;
		previousGradientsBuffer[1] = dy;
		previousGradientsBuffer[2] = dz;
		
		
		//-----RMSprop
		/*double gradX = gamma * previousGradientsBuffer[0] + (1.0-gamma)*dx*dx;
		double gradY = gamma * previousGradientsBuffer[1] + (1.0-gamma)*dy*dy;
		double gradZ = gamma * previousGradientsBuffer[2] + (1.0-gamma)*dz*dz;
		
		previousGradientsBuffer[0] = gradX;
		previousGradientsBuffer[1] = gradY;
		previousGradientsBuffer[2] = gradZ;
		
		dx = dx * descentRate / Math.sqrt(gradX);
		dy = dy * descentRate / Math.sqrt(gradY);
		dz = dz * descentRate / Math.sqrt(gradZ);*/
		
		
		//------Limit the max speed of the convergence. Prevents overshooting.
		if(Math.abs(dx) > maxSgdGradientStepSize) {
			if(dx < 0) dx = -maxSgdGradientStepSize;
				else dx = maxSgdGradientStepSize;
		}
		
		if(Math.abs(dy) > maxSgdGradientStepSize) {
			if(dy < 0) dy = -maxSgdGradientStepSize;
				else dy = maxSgdGradientStepSize;
		}
		
		if(Math.abs(dz) > maxSgdGradientStepSize) {
			if(dz < 0) dz = -maxSgdGradientStepSize;
				else dz = maxSgdGradientStepSize;
		}
		
		return new double[] {dx, dy, dz};
	}
	
	
	
	//---------------------------------------------------Helper functions------------------------------------------------------------
	private int findIndexOfDetectorID(String ID) {
		for(int i=0; i < detectors.size(); i++) {
			if(ID.equals(detectors.get(i).getID())) return i;
		}
		return -1;
	}
	
	public double calculateDistance(double x1, double y1,  double z1, double x2, double y2, double z2) {
		return Math.sqrt((x1-x2)*(x1-x2) + (y1-y2)*(y1-y2) + (z1-z2)*(z1-z2));
	}
	
	public ArrayList<Detector> getDetectors() {
		return detectors;
	}
	
	private Bounds calculateBoundrarys(ArrayList<ReceivingDetector> receivingDetectors) {
		Bounds bounds = new Bounds();
		for(ReceivingDetector detector : receivingDetectors) {
			if(bounds.maxX < detector.getDetectorPosX())  bounds.maxX = detector.getDetectorPosX();
			if(bounds.minX > detector.getDetectorPosX())  bounds.minX = detector.getDetectorPosX();
			if(bounds.maxY < detector.getDetectorPosY())  bounds.maxY = detector.getDetectorPosY();
			if(bounds.minY > detector.getDetectorPosY())  bounds.minY = detector.getDetectorPosY();
			if(bounds.maxZ < detector.getDetectorPosZ())  bounds.maxZ = detector.getDetectorPosZ();
			if(bounds.minZ > detector.getDetectorPosZ())  bounds.minZ = detector.getDetectorPosZ();
		}
		bounds.diameter = Math.sqrt((bounds.maxX-bounds.minX)*(bounds.maxX-bounds.minX) + (bounds.maxY-bounds.minY)*(bounds.maxY-bounds.minY) + (bounds.maxZ-bounds.minZ)*(bounds.maxZ-bounds.minZ));
		return bounds;
	}
	
	
	/*private Position ransacRefinedPosition(Position sgdPosition, ArrayList<ReceivingDetector> receivingDetectors, boolean useFixedZcoordinate, double fixedZcoord) {
		if(receivingDetectors.size() <= 4) return sgdPosition;
		Position prevPosition = sgdPosition;
		double avgX = 0, avgY = 0, avgZ = 0, avgAccuracy = 0, weights = 0;
		
		Random rand = new Random();
		for(int i=0; i < 50; i++) {
			ArrayList<ReceivingDetector> partialReceivingDetectors = new ArrayList<ReceivingDetector>();
			//System.out.println();
			for(int j=0; j < 4; j++) {
				int randomNewBeaconIndex = rand.nextInt(receivingDetectors.size()-1);
				while(isBeaconAppearsOnList(receivingDetectors.get(randomNewBeaconIndex).getDetectorID(), partialReceivingDetectors)) randomNewBeaconIndex = rand.nextInt(receivingDetectors.size()-1);
				partialReceivingDetectors.add(receivingDetectors.get(randomNewBeaconIndex));
				//System.out.print(receivingDetectors.get(randomNewBeaconIndex).getDetectorID() + " ");
			}
			
			Position positionWithSkippedBeacon = positionCalculationWithSGD(partialReceivingDetectors, new Position(prevPosition.getX(), prevPosition.getY(), prevPosition.getZ()), useFixedZcoordinate);
			prevPosition = positionWithSkippedBeacon;
			
			final double weight = 1.0 / positionWithSkippedBeacon.getAccuracy();
			weights += weight;
			avgX += positionWithSkippedBeacon.getX() * weight;
			avgY += positionWithSkippedBeacon.getY() * weight;
			avgZ += positionWithSkippedBeacon.getZ() * weight;
			avgAccuracy += positionWithSkippedBeacon.getAccuracy() * weight;
		}
		
		sgdPosition.setX(avgX / weights);
		sgdPosition.setY(avgY / weights);
		sgdPosition.setZ(avgZ / weights);
		sgdPosition.setAccuracy(avgAccuracy / weights);
		return sgdPosition;
	}
	
	private boolean isBeaconAppearsOnList(String beaconID, ArrayList<ReceivingDetector> partialReceivingDetectors) {
		for(ReceivingDetector receivingDetector : partialReceivingDetectors) if(beaconID.equals(receivingDetector.getDetectorID())) return true;
		return false;
	}*/
	
	
	
	/*private boolean isPositionInsideBoundraries(Position calculatedPosition, Bounds bounds) {
		if(bounds.minX <= calculatedPosition.getX() && bounds.maxX >= calculatedPosition.getX() && 
		   bounds.minY <= calculatedPosition.getY() && bounds.maxY >= calculatedPosition.getY() && 
		   bounds.minZ <= calculatedPosition.getZ() && bounds.maxZ >= calculatedPosition.getZ()) return true;
		else return false;
	}*/
	
	
	/*private Position getClosestDetectorPosition(ArrayList<ReceivingDetector> receivingDetectors) {
		if(receivingDetectors.isEmpty()) return new Position();
		ReceivingDetector closestDetector = receivingDetectors.get(0);
		
		for(ReceivingDetector detector : receivingDetectors) {
			if(closestDetector.getDistance() > detector.getDistance()) closestDetector = detector;
		}
		
		Position closestPosition = new Position();
		closestPosition.setX( closestDetector.getDetectorPosX() );
		closestPosition.setY( closestDetector.getDetectorPosY() );
		closestPosition.setZ( closestDetector.getDetectorPosZ() );
		return closestPosition;
	}*/
	
	
	/*private double calcWeightedAverage(double value1, double value2, double weight1, double weight2) {
		if(Double.isInfinite(weight1)) return value1;
		if(Double.isInfinite(weight2)) return value2;
		return (value1*weight1 + value2*weight2) / (weight1 + weight2);
	}*/
	
	
	/*private Position getBestInitialPosition(ArrayList<ReceivingDetector> receivingDetectors) {
		if(receivingDetectors.isEmpty()) return new Position();
		
		ReceivingDetector closestDetector = receivingDetectors.get(0);
		for(ReceivingDetector detector : receivingDetectors) {
			if(detector.getDistance() < closestDetector.getDistance())  closestDetector = detector;
		}
		
		Position closestPosition = new Position();
		closestPosition.setX( closestDetector.getDetectorPosX() );
		closestPosition.setY( closestDetector.getDetectorPosY() );
		closestPosition.setZ( closestDetector.getDetectorPosZ() );
		return closestPosition;
	}*/
}
