package program;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import data.GPScoordinate;
import data.Position;
import java.net.URISyntaxException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Properties;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import io.socket.client.IO;
import io.socket.client.Socket;
import io.socket.emitter.Emitter;


public class SocketIoSender {
    private final String EMIT_TAG = "message";
    private final String NAMESPACE_TAG = "response";
    private final String PROPERTIES_FILE_PATH = "socketBuffer.properties";
	private final Properties PROPERTIES;
	
	private final Socket SOCKET;
	
	private final ArrayList<String> BUFFER;
	private Thread sendingThread;
	private boolean sendingInProgress = false;
	
	
    
    public SocketIoSender() throws URISyntaxException {
    	 PROPERTIES = Values.loadOrCreatePropertiesFile(PROPERTIES_FILE_PATH);
    	 BUFFER = readSendBufferFromFile();
    	
    	 IO.Options socketOptions = new IO.Options();
         socketOptions = new IO.Options();
         socketOptions.secure = true;
         socketOptions.reconnection = true;
         socketOptions.timeout = Values.getApiTimeout();
         
         try {
             SSLContext sslContext = SSLContext.getInstance("SSL");
             sslContext.init(null, trustAllCerts, null);
         } catch (Exception e) {
             e.printStackTrace();
         }
         
         //----------Front end socket----------
         SOCKET = IO.socket(Values.getSocketAddress(), socketOptions);
         SOCKET.connect();
         
         SOCKET.on("connect", new Emitter.Listener() {
             @Override
             public void call(Object... args) {
                 System.out.println("BN " + "Socket CONNECTED");
                 if(!BUFFER.isEmpty()) sendWithSocket(BUFFER.get(0));
             }
         });
         SOCKET.on("disconnect", new Emitter.Listener() {
             @Override
             public void call(Object... args) {
                 System.out.println("BN " + "Sockett DISCONNECTED");
             }
         });
         SOCKET.on("reconnect", new Emitter.Listener() {
             @Override
             public void call(Object... args) {
                 System.out.println("BN " + "Socket RECONNECTING");
             }
         });
         SOCKET.on("error", new Emitter.Listener() {
             @Override
             public void call(Object... args) {
             	System.out.println("BN " + "Socket ERROR");
             }
         });
         
         SOCKET.on(NAMESPACE_TAG, new Emitter.Listener() {
             @Override
             public void call(Object... args) {
             	JSONObject response = (JSONObject) args[0];
             	System.out.println("Socket response: " + response.toString());
             	
             	if(isResponseValid(response)) {
             		if(!BUFFER.isEmpty()) BUFFER.remove(0);
             		saveBufferToFile();
             		sendingInProgress = false;
             		if(!BUFFER.isEmpty()) sendWithSocket(BUFFER.get(0));
             	}
             }
         });
         
         //startSenderThread();
    }
    
    
    /*private void startSenderThread() {
    	sendingThread = new Thread() {
			public void run() {
				while(true) {
					if(!BUFFER.isEmpty() && !sendingInProgress && SOCKET.connected()) {
						sendWithSocket(BUFFER.get(0));
					}
					try {
						Thread.sleep(100);
					} catch (InterruptedException e) { e.printStackTrace(); }
				}
			}
		};
		sendingThread.start();
    }*/
    
    
    public void send(ArrayList<Position> positions) {
    	BUFFER.add(positionsListToString(positions));
    	if(BUFFER.size() == 1) sendWithSocket(BUFFER.get(0));		//If there is no sending in progress, send the buffer content one by one.
        saveBufferToFile();
    }
    
    
    private void sendWithSocket(String data) {
    	sendingInProgress = true;
    	System.out.println("SOCKET_SEND: " + EMIT_TAG + "  " + data);
        SOCKET.emit(EMIT_TAG, data);
    }
    
    
	private boolean isResponseValid(JSONObject response) {
		try {
			return response.get("status").equals("OK");
		} catch(Exception e) {
			return false;
		}
	}
	
	
	private String positionsListToString(ArrayList<Position> positions) {
		JSONObject json = new JSONObject();
		try {
			json.put("timestamp", System.currentTimeMillis());
			json.put("client_id", Values.getClientID());
			json.put("type", "position");
			json.put("package", createPackageString());
			
			JSONObject data = new JSONObject();
			JSONArray posArray = new JSONArray();
			
			for(Position position : positions) {
				JSONObject element = new JSONObject();
				element.put("tag_id", position.getBeaconID());
				element.put("timestamp", position.getTimestamp());
				
				JSONObject relativePosition = new JSONObject();
				relativePosition.put("x", position.getX());
				relativePosition.put("y", position.getY());
				relativePosition.put("z", position.getZ());
				relativePosition.put("accuracy", position.getAccuracy());
				element.put("relative_location", relativePosition);
				
				JSONObject geolocation = new JSONObject();
				if(Values.getOrigoOfTheRelativeCoordinateSystem() != null) {
					GPScoordinate convertedGPScoordinate = new GPScoordinate().convertRelativeCoordinateToGPS(
							Values.getOrigoOfTheRelativeCoordinateSystem(), Values.getRelativeCoordinateSystemRoatationAngle(), position);
					geolocation.put("latitude", convertedGPScoordinate.getLatitude());
					geolocation.put("longitude", convertedGPScoordinate.getLongitude());
				}
				element.put("geolocation", geolocation);
				
				posArray.put(element);
			}
			
			data.put("positions", posArray);
			json.put("data", data);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return json.toString();
	}
	
	
	private String createPackageString() {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy_MM_dd_ss_SS");
		return Values.getClientID() + sdf.format(new Date()) + "00_000";
	}
	
	
	
	private void saveBufferToFile() {
		PROPERTIES.setProperty("sendBuffer", new Gson().toJson(BUFFER));
		Values.writePropertiesFile(PROPERTIES, PROPERTIES_FILE_PATH);
	}
	
	private ArrayList<String> readSendBufferFromFile() {
		ArrayList<String> buffer = new ArrayList<String>();
		try {
			String serialisedBuffer = PROPERTIES.getProperty("sendBuffer", "");
			buffer = new Gson().fromJson(serialisedBuffer, new TypeToken<List<String>>(){}.getType());
			if(buffer != null && !buffer.isEmpty()) return new ArrayList<String>();
		} catch (Exception e) { e.printStackTrace(); }
		return buffer;
	}
	
	
	private final TrustManager[] trustAllCerts = new TrustManager[] { new X509TrustManager() {
        public java.security.cert.X509Certificate[] getAcceptedIssuers() {
            return new java.security.cert.X509Certificate[] {};
        }
        public void checkClientTrusted(X509Certificate[] chain, String authType) throws CertificateException {}
        public void checkServerTrusted(X509Certificate[] chain, String authType) throws CertificateException {}
    }};
}
