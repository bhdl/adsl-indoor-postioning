package program;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Iterator;
import org.json.JSONObject;
import buffer.Buffer;
import data.DetectedBeacon;
import data.ReceivedSignal;


public final class API {
	private final String GET = "GET";
	private final String POST = "POST";
	
	private final Thread POLLING_THREAD;
	private final Buffer BUFFER = new Buffer();
	
	private ArrayList<DetectedBeacon> detectedBeacons;
	
	
	public API() {
		POLLING_THREAD = new Thread() {
			public void run() {
				startPolling();
			}
		};
		POLLING_THREAD.start();
	}
	
	
	private void startPolling() {
		while(true) {
			detectedBeacons = getChangesAPIcall();
			BUFFER.addMeasurement(detectedBeacons);
			
			try {
				Thread.sleep(Values.getPollingTime());
			} catch (InterruptedException e) { e.printStackTrace(); }
		}
	}
	
	public void stopPolling() {
		POLLING_THREAD.stop();
	}
	
	
	public ArrayList<DetectedBeacon> getAveragedBeaconMeasurements() {
		return BUFFER.calcAveragedBeaconMeasurements();
	}
	
	public ArrayList<DetectedBeacon> getLatestBeaconMeasurements() {
		return detectedBeacons;
	}
	
	
	private ArrayList<DetectedBeacon> getChangesAPIcall() {
		JSONObject response = apiCall(Values.getApiURL(), GET, "", false);
		return parseBeaconsData(response);
	}
	
	
	private ArrayList<DetectedBeacon> parseBeaconsData(JSONObject response) {
		ArrayList<DetectedBeacon> detectedBeacons = new ArrayList<DetectedBeacon>();
		if(response == null) return detectedBeacons;
		
		try {
			JSONObject data = response.getJSONObject("data");
			
			Iterator beaconsIterator = data.keys();
			while(beaconsIterator.hasNext()) {
				String beaconID = (String) beaconsIterator.next();
				
				DetectedBeacon beacon = new DetectedBeacon();
				beacon.setID(beaconID);
				beacon.setDetectionTimestamp(System.currentTimeMillis());
				
				JSONObject beaconJSON = data.getJSONObject(beaconID);
				Iterator detectorsIterator = beaconJSON.keys();
				while(detectorsIterator.hasNext()) {
					String detectorID = (String) detectorsIterator.next();
					
					ReceivedSignal signal = new ReceivedSignal();
					signal.setBeaconID(beaconID);
					signal.setDetectorID(detectorID);
					signal.setDistance(beaconJSON.getDouble(detectorID));
					signal.setTimestamp(System.currentTimeMillis());
					
					beacon.getReceivingDetectors().add(signal);
				}
				
				if(!beacon.getReceivingDetectors().isEmpty()) detectedBeacons.add(beacon);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return detectedBeacons;
	}
	
	
	
	private void printDetectors(ArrayList<DetectedBeacon> detectedBeacons) {
		for(DetectedBeacon detectedBeacon : detectedBeacons) System.out.println(detectedBeacon.toString());
	}
	
	
	private JSONObject apiCall(String urlAddress, String requestMethod, String message, boolean doOutput) {
		System.out.println("API send: " + message + "  " + urlAddress);
		try {
            URL url = new URL(urlAddress);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setReadTimeout(Values.getApiTimeout());
            conn.setConnectTimeout(Values.getApiTimeout());
            conn.setRequestMethod(requestMethod);
            conn.setDoInput(true);
            conn.setDoOutput(doOutput);
            if(doOutput) {
            	conn.setRequestProperty("Content-Type", "application/json");
                conn.getOutputStream().write(message.getBytes("UTF-8"));
            }
            
            BufferedReader br = null;
            if (conn.getResponseCode() == HttpURLConnection.HTTP_OK) {
                br = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            } else {
                br = new BufferedReader(new InputStreamReader(conn.getErrorStream()));
                System.out.println("API error: " + conn.getResponseCode());
            }
            
            String line;
            String read = "";
            if (br != null) {
            	while ((line = br.readLine()) != null) {
                	read += line;
                }
            }
            conn.disconnect();
            System.out.println("API response: " + read);
            return new JSONObject(read);
		} catch (Exception e) {
			System.err.println("Error in api call!");
			e.printStackTrace();
			return null;
		}
	}
}
