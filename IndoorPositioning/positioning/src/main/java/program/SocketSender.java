package program;

import java.net.*;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Properties;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import data.GPScoordinate;
import data.Position;
import java.io.*;


public class SocketSender {
	private final String PROPERTIES_FILE_PATH = "socketBuffer.properties";
	private final Properties PROPERTIES;
	
	private final Thread SOCKET_THREAD;
	
	private Socket socket;
	private PrintWriter out;
	private BufferedReader in;
	
	private boolean socketInitialised = false;
	private boolean dataSendingInProgress = false;
	private ArrayList<String> sendBufferFIFO = new ArrayList<String>();
	
	
	
	public SocketSender() {
		PROPERTIES = Values.loadOrCreatePropertiesFile(PROPERTIES_FILE_PATH);
		
		SOCKET_THREAD = new Thread() {
			public void run() {
				initialiseSocket();
				initialiseSendingThread();
			}
		};
		SOCKET_THREAD.start();
	}
	
	
	private void initialiseSocket() {
		socketInitialised = false;
		while(!socketInitialised) {
			try {
				socket = new Socket(Values.getSocketAddress(), Values.getSocketPort());
				socket.setSoTimeout(Values.getSocketTimeout());
				out = new PrintWriter(socket.getOutputStream(), true);
				in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
				socketInitialised = true;
				
				readSendBufferFromFile();
				sendBufferContent();
			} catch (IOException e) {
				socketInitialised = false;
				e.printStackTrace();
			}
		}
	}
	
	
	private void initialiseSendingThread() {
		while(true) {
			if(socketInitialised && !sendBufferFIFO.isEmpty()) sendBufferContent();
			try {
				Thread.sleep(100);
			} catch (InterruptedException e) { e.printStackTrace(); }
		}
	}
	
	
	public void closeSocket() {
		socketInitialised = false;
		out.close();
		try {
			in.close();
		} catch (IOException e) {}
		try {
			socket.close();
		} catch (IOException e) {}
	}
	
	public void closeAll() {
		closeSocket();
		SOCKET_THREAD.stop();
	}
	
	private void resetSocket() {
		closeSocket();
		initialiseSocket();
	}
	
	
	public void addToSendBuffer(ArrayList<Position> positions) {
		sendBufferFIFO.add(positionsListToString(positions));
		if(!socketInitialised) saveBufferToFile();
	}
	
	
	private void sendBufferContent() {
		if(sendBufferFIFO.isEmpty() || !socketInitialised || dataSendingInProgress) return;
		
		dataSendingInProgress = true;
		try {
			String response = sendWithSocket(sendBufferFIFO.get(0));
			if(isResponseValid(response)) sendBufferFIFO.remove(0);
		} catch (Exception e) {
			System.err.println("ERROR: Socket timeout! Resetting connection!");
			resetSocket();
		}
		
		saveBufferToFile();
		dataSendingInProgress = false;
		
		if(!sendBufferFIFO.isEmpty()) sendBufferContent();
	}
	
	
	
	private String sendWithSocket(String message) throws IOException {
		out.println(message);						//Send through the socket
		return in.readLine();						//Wait for a response for max T sec.
	}
	
	
	private boolean isResponseValid(String response) {
		try {
			//return true;							//Use this for testing
			return new JSONObject(response).get("status").equals("OK");
		} catch(Exception e) {
			return false;
		}
	}
	
	
	
	private String positionsListToString(ArrayList<Position> positions) {
		JSONObject json = new JSONObject();
		try {
			json.put("timestamp", System.currentTimeMillis());
			json.put("client_id", Values.getClientID());
			json.put("type", "position");
			json.put("package", createPackageString());
			
			JSONObject data = new JSONObject();
			JSONArray posArray = new JSONArray();
			
			for(Position position : positions) {
				JSONObject element = new JSONObject();
				element.put("tag_id", position.getBeaconID());
				element.put("timestamp", position.getTimestamp());
				
				JSONObject relativePosition = new JSONObject();
				relativePosition.put("x", position.getX());
				relativePosition.put("y", position.getY());
				relativePosition.put("z", position.getZ());
				relativePosition.put("accuracy", position.getAccuracy());
				element.put("relative_location", relativePosition);
				
				JSONObject geolocation = new JSONObject();
				if(Values.getOrigoOfTheRelativeCoordinateSystem() != null) {
					GPScoordinate convertedGPScoordinate = new GPScoordinate().convertRelativeCoordinateToGPS(
							Values.getOrigoOfTheRelativeCoordinateSystem(), Values.getRelativeCoordinateSystemRoatationAngle(), position);
					geolocation.put("latitude", convertedGPScoordinate.getLatitude());
					geolocation.put("longitude", convertedGPScoordinate.getLongitude());
				}
				element.put("geolocation", geolocation);
				
				posArray.put(element);
			}
			
			data.put("positions", posArray);
			json.put("data", data);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return json.toString();
	}
	
	
	private String createPackageString() {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy_MM_dd_ss_SS");
		return Values.getClientID() + sdf.format(new Date()) + "00_000";
	}
	
	
	
	//-----Config file-----
	private void saveBufferToFile() {
		PROPERTIES.setProperty("sendBuffer", new Gson().toJson(sendBufferFIFO));
		Values.writePropertiesFile(PROPERTIES, PROPERTIES_FILE_PATH);
	}
	
	private void readSendBufferFromFile() {
		try {
			String serialisedBuffer = PROPERTIES.getProperty("sendBuffer", "");
			ArrayList<String> buffer = new Gson().fromJson(serialisedBuffer, new TypeToken<List<String>>(){}.getType());
			if(buffer != null && !buffer.isEmpty()) sendBufferFIFO = buffer;
		} catch (Exception e) { e.printStackTrace(); }
	}
}