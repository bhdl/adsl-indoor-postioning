package program;

import java.awt.Dimension;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Random;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.MatOfByte;
import org.opencv.core.Point;
import org.opencv.core.Scalar;
import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.imgproc.Imgproc;
import data.DetectedBeacon;
import data.Detector;
import data.Position;
import data.ReceivedSignal;
import data.ReceivingDetector;


public class Plotting {
	private double zMin = 0;
	public double z = zMin;
	private double zMax = 2;
	private double zSteps = 0.05;
	private double minZvalue = Double.MAX_VALUE;
	private double dstAverage = 0, dstCounter = 0;
	
	
	public Plotting() {
		System.loadLibrary(Core.NATIVE_LIBRARY_NAME);
	}
	
	
	public void tomographicPlotting() {
		//Only for printing
		/*if(simulationMode) {
			dstAverage += POSITIONING.calculateDistance(calcPosition.getX(), calcPosition.getY(), calcPosition.getZ(),  simulatedPosition.getX(),  simulatedPosition.getY(),  simulatedPosition.getZ());
			dstCounter++;
		}*/
		z += zSteps;
		if(z > zMax) z = zMin;
		//if(minZvalue > calcPosition.getAccuracy()) minZvalue = calcPosition.getAccuracy();
		//System.out.println("Calc position: " + calcPosition);
		//if(simulationMode) System.out.println("AVG: " + dstAverage / dstCounter);
	}
	
	
	public void printResults(Position realPosition, ArrayList<Position> calculatedPositions, ArrayList<DetectedBeacon> detectedBeacons, SpatialTrilateration positioning) {
		int newHeight = 1000;
		Mat printedImage = null;
		Random rand = new Random(3);
		
		for(int i=0; i < detectedBeacons.size(); i++) {
			ArrayList<ReceivedSignal> detectedTags = detectedBeacons.get(i).getReceivingDetectors();
			
			Position sgdPosition = calculatedPositions.get(i);
			if(sgdPosition == null) continue;
			
			ArrayList<ReceivingDetector> receivingDetectors = positioning.assignReceivedDetectors(detectedTags);
			if(receivingDetectors.isEmpty()) continue;
			
			double detectorMaxWidth = 0, detectorMaxHeight = 0;
			for(Detector detector : positioning.getDetectors()) {
				if(detector.getX() > detectorMaxWidth) detectorMaxWidth = detector.getX();
				if(detector.getY() > detectorMaxHeight) detectorMaxHeight = detector.getY();
			}
			
			double z = sgdPosition.getZ();
			double multiplier = detectorMaxHeight / (double)newHeight;
			int newWidth = (int) Math.round(detectorMaxWidth / multiplier);
			if(printedImage == null) printedImage = new Mat((int)(newHeight * 1.05f), (int)(newWidth * 1.05f) + 20, CvType.CV_32FC3);
			
			
			//-----Print the probability function-----
			if(i == 0) {
				double minDistDiff = Double.MAX_VALUE;
				for(int x = 0; x < printedImage.width(); x++) {
					for(int y = 0; y < printedImage.height(); y++) {
						double samplingX = x * multiplier;
						double samplingY = y * multiplier;
						double distDiffs = positioning.calculateTotalDistanceDiffBetweenPositionAndMeasuredDistances(samplingX, samplingY, z, receivingDetectors);
						printedImage.put(y, x, new double[]{distDiffs, distDiffs, distDiffs});
						if(distDiffs <= minDistDiff) minDistDiff = distDiffs;
					}
				}
				//Core.multiply(printedImage, new Scalar(1.0/minDistDiff, 1.0/minDistDiff, 1.0/minDistDiff), printedImage);
				Core.multiply(printedImage, new Scalar(100, 100, 100), printedImage);
			}
			
			
			int red = 50+rand.nextInt(205), green = 50+rand.nextInt(205), blue = 50+rand.nextInt(205);
			for(ReceivingDetector detector : receivingDetectors) {
				//Print the detector position
				Imgproc.circle(printedImage, new Point(detector.getDetectorPosX()/multiplier, detector.getDetectorPosY()/multiplier), 2, new Scalar(255, 0, 255), 1);
				
				double radius = detector.getDistance();
				radius = Math.sqrt( radius*radius - (z-detector.getDetectorPosZ())*(z-detector.getDetectorPosZ()) );
				//Print a circle, with the radius of the detection distance
				Imgproc.circle(printedImage, new Point(detector.getDetectorPosX()/multiplier, detector.getDetectorPosY()/multiplier), (int)(radius/multiplier), new Scalar(red, green, blue), 1);
				
				Imgproc.putText(printedImage, detector.getDetectorID(), new Point(detector.getDetectorPosX()/multiplier - 15, detector.getDetectorPosY()/multiplier), Core.FONT_HERSHEY_PLAIN, 1, new Scalar(255, 0, 0), 2);
			}
			
			//Min error pos
			Imgproc.circle(printedImage, new Point(sgdPosition.getX()/multiplier, sgdPosition.getY()/multiplier), 2, new Scalar(red*2, green*2, blue*2), 2);
			
			//Accuracy
			Imgproc.circle(printedImage, new Point(sgdPosition.getX()/multiplier, sgdPosition.getY()/multiplier), (int)(sgdPosition.getAccuracy()/multiplier), new Scalar(red*2, green*2, blue*2), 2);
			
			//Real position for simulation
			if(realPosition != null) Imgproc.circle(printedImage, new Point(realPosition.getX()/multiplier, realPosition.getY()/multiplier), 2, new Scalar(255, 0, 0), 2);
		}
		
		
		//Mirror image
		if(printedImage != null) {
			Core.flip(printedImage, printedImage, 1);
			//Print the height
			//Imgproc.putText(printedImage, (new DecimalFormat("00.00")).format(sgdPosition.getZ()) + " m    Best Z: " + bestZvalue + "  Min val: " + minZvalue, new Point(10, 50), Core.FONT_HERSHEY_SIMPLEX, 1, new Scalar(255, 0, 0), 2);
			showImageInPlayer(printedImage);
		}
	}
	
	
	
	private JLabel videoPlayerFrame;
	
	private void setPlayerWindow(int width, int height) {
		JFrame jframe = new JFrame("Title");
	    jframe.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	    videoPlayerFrame = new JLabel();
	    jframe.setContentPane(videoPlayerFrame);
	    jframe.setVisible(true);
	    jframe.setSize(new Dimension(width, height));
	}
	
	private void showImageInPlayer(Mat img) {
		if(videoPlayerFrame == null) setPlayerWindow(img.width(), img.height());
		videoPlayerFrame.setIcon(new ImageIcon(Mat2BufferedImage(img)));
		videoPlayerFrame.repaint();
	}
	
	private BufferedImage Mat2BufferedImage(Mat matrix) {        
	    MatOfByte mob = new MatOfByte();
	    Imgcodecs.imencode(".jpg", matrix, mob);
	    byte ba[] = mob.toArray();
	    BufferedImage bi = null;
		try {
			bi = ImageIO.read(new ByteArrayInputStream(ba));
		} catch (IOException e) { e.printStackTrace(); }
	    return bi;
	}
}
