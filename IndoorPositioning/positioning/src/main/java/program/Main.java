package program;

import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import data.DetectedBeacon;
import data.Detector;
import data.Position;
import test.Simulator;


public class Main {
	private static final String DETECTORS_FILE = "detectors.properties";
	private static boolean keepPositioning = true;
	
	private static boolean simulationMode = false;
	private static Position simulatedPosition = new Position(10, 15, 4);
	
	
	public static void main(String[] args) throws URISyntaxException {
		Values.readAllValuesFromConfig();
		
		final SocketIoSender SOCKET = new SocketIoSender();
		final API DATA_SOURCE = new API();
		
		final ArrayList<Detector> DETECTORS = (!simulationMode) ? readDetectorsFromFile(DETECTORS_FILE) : Simulator.getSimulatedDetectorsList();
		System.out.println(DETECTORS.size());
		final SpatialTrilateration POSITIONING = new SpatialTrilateration(DETECTORS);
		
		final Plotting PLOTTING = new Plotting();
		
		while(keepPositioning) {
			//long start = System.currentTimeMillis();
			
			ArrayList<DetectedBeacon> detectedBeacons = new ArrayList<DetectedBeacon>();
			if(!simulationMode) detectedBeacons = DATA_SOURCE.getAveragedBeaconMeasurements();
				else detectedBeacons.add( Simulator.simulateDetectedBeacon(DETECTORS, simulatedPosition) );
			
			ArrayList<Position> calculatedPositions = new ArrayList<Position>();
			
			for(DetectedBeacon beacon : detectedBeacons) {
				//Position calcPosition = new Position(0, 0, PLOTTING.z);					//Only for tomographic printing
				//Position calcPosition = POSITIONING.calculatePosition(beacon.getReceivingDetectors(), PLOTTING.z);
				Position calcPosition = POSITIONING.calculatePosition(beacon.getReceivingDetectors(), true);
				PLOTTING.tomographicPlotting();
				if(calcPosition != null) calculatedPositions.add(calcPosition);
			}
			
			//System.out.println((System.currentTimeMillis() - start) + " ms");
			
			SOCKET.send(calculatedPositions);
			PLOTTING.printResults(simulatedPosition, calculatedPositions, detectedBeacons, POSITIONING);
			
			try {
				Thread.sleep(Values.getPollingTime());
			} catch (InterruptedException e) { e.printStackTrace(); }
		}
	}
	
	
	private static ArrayList<Detector> readDetectorsFromFile(String file) {
		String serialisedDetectors = Values.getKeyFromProperties(Values.loadOrCreatePropertiesFile(file), "detectors", "");
		try {
			return new Gson().fromJson(serialisedDetectors, new TypeToken<List<Detector>>(){}.getType());
		} catch(Exception e) {
			System.err.println("ERROR: Cannot parse detectors from file!");
			return new ArrayList<Detector>();
		}
	}
}
