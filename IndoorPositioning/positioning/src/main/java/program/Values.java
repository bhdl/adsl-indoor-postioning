package program;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Properties;
import com.google.gson.Gson;
import data.GPScoordinate;


public final class Values {
	private Values() {}
	
	private static final String PROPERTIES_PATH = "settings.properties";
	private static final Properties PROPERTIES = loadOrCreatePropertiesFile(PROPERTIES_PATH);
	private static String clientID = "ips01";
	
	//Socket variables
	private static String socketAddress = "http://127.0.0.1:4000/connection/input";	//"localhost";
	private static int socketPort = 5555;
	private static int socketTimeout = 2500;
	
	//API variables
	private static String apiURL = "http://192.168.0.202:2000/changes/";
	private static int apiTimeout = 5000;
	private static int pollingTime = 500;
	
	private static long signalsBufferIntervall = 10000;
	
	//Coordinate system conversion
	private static float relativeCoordinateSystemRoatationAngle;
	private static GPScoordinate origoOfTheRelativeCoordinateSystem = null;
	
	
	public static void readAllValuesFromConfig() {
		clientID = getKeyFromProperties(PROPERTIES, "clientID", clientID);
		socketAddress = getKeyFromProperties(PROPERTIES, "socketAddress", socketAddress);
		socketPort = getKeyFromProperties(PROPERTIES, "socketPort", socketPort);
		socketTimeout = getKeyFromProperties(PROPERTIES, "socketTimeout", socketTimeout);
		apiURL = getKeyFromProperties(PROPERTIES, "apiURL", apiURL);
		apiTimeout = getKeyFromProperties(PROPERTIES, "apiTimeout", apiTimeout);
		pollingTime = getKeyFromProperties(PROPERTIES, "pollingTime", pollingTime);
		signalsBufferIntervall = getKeyFromProperties(PROPERTIES, "signalsBufferIntervall", signalsBufferIntervall);
		relativeCoordinateSystemRoatationAngle = getKeyFromProperties(PROPERTIES, "relativeCoordinateSystemAngleComparedToEarth", relativeCoordinateSystemRoatationAngle);
		try {
			origoOfTheRelativeCoordinateSystem = new Gson().fromJson(getKeyFromProperties(PROPERTIES, "origoOfTheRelativeCoordinateSystem", ""), GPScoordinate.class);
		} catch(Exception e) { e.printStackTrace(); }
	}
	
	
	public static int getKeyFromProperties(Properties properties, String key, int value) {
		return Integer.parseInt(getKeyFromProperties(properties, key, value+""));
	}
	public static long getKeyFromProperties(Properties properties, String key, long value) {
		return Long.parseLong(getKeyFromProperties(properties, key, value+""));
	}
	public static float getKeyFromProperties(Properties properties, String key, float value) {
		return Float.parseFloat(getKeyFromProperties(properties, key, value+""));
	}
	public static boolean getKeyFromProperties(Properties properties, String key, boolean value) {
		return Boolean.parseBoolean(getKeyFromProperties(properties, key, value+""));
	}
	
	
	public static String getKeyFromProperties(Properties properties, String key, String value) {
		return properties.getProperty(key, value);
	}
	
	
	public static Properties loadOrCreatePropertiesFile(String propertiesFilePath) {
		Properties properties = new Properties();
		try {
			InputStream input = new FileInputStream(propertiesFilePath);
			properties.load(input);
		} catch (Exception e) {
			System.err.println("ERROR: Cant open properties file, creating one instead!");
			try {
				properties.store(new FileOutputStream(new File(propertiesFilePath)), "");
			} catch (Exception e1) { e1.printStackTrace(); }
		}
		return properties;
	}
	
	
	public static boolean writePropertiesFile(Properties properties, String propertiesFilePath) {
		try {
			File file = new File(propertiesFilePath);
			FileOutputStream os = new FileOutputStream(file);
			properties.store(os, "");
			os.close();
			return true;
		} catch (Exception e) { e.printStackTrace(); }
		return false;
	}
	
	
	
	public static String getClientID() {
		return clientID;
	}
	public static void setClientID(String clientID) {
		Values.clientID = clientID;
	}
	
	public static String getSocketAddress() {
		return socketAddress;
	}
	public static void setSocketAddress(String socketAddress) {
		Values.socketAddress = socketAddress;
	}
	
	public static int getSocketPort() {
		return socketPort;
	}
	public static void setSocketPort(int socketPort) {
		Values.socketPort = socketPort;
	}
	
	public static int getSocketTimeout() {
		return socketTimeout;
	}
	public static void setSocketTimeout(int socketTimeout) {
		Values.socketTimeout = socketTimeout;
	}
	
	public static String getApiURL() {
		return apiURL;
	}
	public static void setApiURL(String apiURL) {
		Values.apiURL = apiURL;
	}
	
	public static int getApiTimeout() {
		return apiTimeout;
	}
	public static void setApiTimeout(int apiTimeout) {
		Values.apiTimeout = apiTimeout;
	}
	
	public static int getPollingTime() {
		return pollingTime;
	}
	public static void setPollingTime(int pollingTime) {
		Values.pollingTime = pollingTime;
	}
	
	public static long getSignalsBufferIntervall() {
		return signalsBufferIntervall;
	}
	public static void setSignalsBufferIntervall(long signalsBufferIntervall) {
		Values.signalsBufferIntervall = signalsBufferIntervall;
	}
	
	public static float getRelativeCoordinateSystemRoatationAngle() {
		return relativeCoordinateSystemRoatationAngle;
	}
	public static void setRelativeCoordinateSystemRoatationAngle(float relativeCoordinateSystemRoatationAngle) {
		Values.relativeCoordinateSystemRoatationAngle = relativeCoordinateSystemRoatationAngle;
	}
	
	public static GPScoordinate getOrigoOfTheRelativeCoordinateSystem() {
		return origoOfTheRelativeCoordinateSystem;
	}
	public static void setOrigoOfTheRelativeCoordinateSystem(GPScoordinate origoOfTheRelativeCoordinateSystem) {
		Values.origoOfTheRelativeCoordinateSystem = origoOfTheRelativeCoordinateSystem;
	}
}
