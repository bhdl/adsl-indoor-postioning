package test;

import java.util.ArrayList;
import java.util.Random;
import data.DetectedBeacon;
import data.Detector;
import data.Position;
import data.ReceivedSignal;
import program.SpatialTrilateration;


public final class Simulator {
	private Simulator() {}
	
	private static final float simulatedDistanceFluctuation = 0.5f;
	private static final Random rand = new Random(7);
	
	/*public static void main(String[] args) {
		testPositioning(new Position(500, 500, 500));
	}*/
	
	
	private static Position testPositioning(final Position realPosition) {
		System.out.println("Real position: " + realPosition);
		final ArrayList<Detector> detectors = getSimulatedDetectorsList();
		long start = System.currentTimeMillis();
		
		SpatialTrilateration positioning = new SpatialTrilateration(detectors);
		Position calcPosition = positioning.calculatePosition(simulateDetectedBeacon(detectors, realPosition).getReceivingDetectors());
		
		System.out.println("Calculation time: " + (System.currentTimeMillis() - start));
		System.out.println("Calc position: " + calcPosition);
		System.out.println("Real accuarcy: " + calculateDistance(realPosition.getX(), realPosition.getY(), realPosition.getZ(), calcPosition.getX(), calcPosition.getY(), calcPosition.getZ()) );
		return calcPosition;
	}
	
	
	public static ArrayList<Detector> getSimulatedDetectorsList() {
		final ArrayList<Detector> detectors = new ArrayList<Detector>();
		
		final Detector d1 = new Detector();
		d1.setID("1");
		d1.setX(0);
		d1.setY(0);
		d1.setZ(15);
		detectors.add(d1);
		
		final Detector d2 = new Detector();
		d2.setID("2");
		d2.setX(0);
		d2.setY(50);
		d2.setZ(10);
		detectors.add(d2);
		
		final Detector d3 = new Detector();
		d3.setID("3");
		d3.setX(40);
		d3.setY(30);
		d3.setZ(5);
		detectors.add(d3);
		
		final Detector d4 = new Detector();
		d4.setID("4");
		d4.setX(20);
		d4.setY(0);
		d4.setZ(1);
		detectors.add(d4);
		
		final Detector d5 = new Detector();
		d5.setID("5");
		d5.setX(25);
		d5.setY(28);
		d5.setZ(4.5);
		detectors.add(d5);
		
		final Detector d6 = new Detector();
		d6.setID("6");
		d6.setX(45);
		d6.setY(19);
		d6.setZ(10);
		detectors.add(d6);
		
		return detectors;
	}
	
	
	public static DetectedBeacon simulateDetectedBeacon(ArrayList<Detector> detectors, Position realPosition) {
		ArrayList<ReceivedSignal> receivedSignals = new ArrayList<ReceivedSignal>();
		
		for(Detector detector : detectors) {
			//Random rand = new Random(123);
			double distanceFromDetector = calculateDistance(detector.getX(), detector.getY(), detector.getZ(),  realPosition.getX(), realPosition.getY(), realPosition.getZ());
			//distanceFromDetector += rand.nextDouble() * simulatedDistanceFluctuation - simulatedDistanceFluctuation/2.0;
			distanceFromDetector += rand.nextDouble() * simulatedDistanceFluctuation;
			
			ReceivedSignal signal = new ReceivedSignal();
			signal.setDetectorID( detector.getID() );
			signal.setDistance( distanceFromDetector );
			
			receivedSignals.add(signal);
		}
		
		/*ReceivedSignal signal = receivedSignals.get(0);
		signal.setDistance( signal.getDistance() + 0 );
		signal.setAccuracy(1);
		receivedSignals.set(0, signal);*/
		
		DetectedBeacon simulatedBeacon = new DetectedBeacon();
		simulatedBeacon.setID("1");
		simulatedBeacon.setDetectionTimestamp(System.currentTimeMillis());
		simulatedBeacon.setReceivingDetectors(receivedSignals);
		
		return simulatedBeacon;
	}
	
	/*private static double calcAccuracy(double distance, double distanceUncertancy) {
		//double acc = (distance - distanceUncertancy) / distance;
		double acc = 1.0 - (distanceUncertancy / distance);
		return acc * acc;
	}*/
	
	public static double calculateDistance(double x1, double y1, double z1,  double x2, double y2, double z2) {
		return Math.sqrt((x1-x2)*(x1-x2) + (y1-y2)*(y1-y2) + (z1-z2)*(z1-z2));
	}
}
