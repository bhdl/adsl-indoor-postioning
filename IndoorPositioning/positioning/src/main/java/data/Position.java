package data;

public class Position {
	private String beaconID;
	private double x;
	private double y;
	private double z;
	private double accuracy;
	private long timestamp;
	

	public Position() {}
	
	public Position(double x, double y, double z) {
		this.x = x;
		this.y = y;
		this.z = z;
	}
	
	public Position(String beaconID, double x, double y, double z) {
		this.beaconID = beaconID;
		this.x = x;
		this.y = y;
		this.z = z;
	}
	
	public Position(String beaconID, double x, double y, double z, double accuracy) {
		this.beaconID = beaconID;
		this.x = x;
		this.y = y;
		this.z = z;
		this.accuracy = accuracy;
	}
	
	public Position(String beaconID, double x, double y, double z, double accuracy, long timestamp) {
		this.beaconID = beaconID;
		this.x = x;
		this.y = y;
		this.z = z;
		this.accuracy = accuracy;
		this.timestamp = timestamp;
	}
	
	
	public Position copy() {
		return new Position(this.beaconID, this.x, this.y, this.z, this.accuracy, this.timestamp);
	}
	
	public String toString() {
		return "X: " + x + "  Y: " + y + "  Z: " + z + "  Acc: " + accuracy + "  Timestamp: " + timestamp;
	}
	
	
	public String getBeaconID() {
		return beaconID;
	}
	public void setBeaconID(String beaconID) {
		this.beaconID = beaconID;
	}
	
	public double getX() {
		return x;
	}
	public void setX(double x) {
		this.x = x;
	}

	public double getY() {
		return y;
	}
	public void setY(double y) {
		this.y = y;
	}

	public double getZ() {
		return z;
	}
	public void setZ(double z) {
		this.z = z;
	}

	public double getAccuracy() {
		return accuracy;
	}
	public void setAccuracy(double value) {
		this.accuracy = value;
	}
	
	public long getTimestamp() {
		return timestamp;
	}
	public void setTimestamp(long timestamp) {
		this.timestamp = timestamp;
	}
}