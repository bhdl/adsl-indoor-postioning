package data;

public class ReceivingDetector {
	private String detectorID = "";
	private double distance;
	
	private double detectorPosX;
	private double detectorPosY;
	private double detectorPosZ;
	
	private double accuracy = 1;
	
	
	public double getAccuracy() {
		return accuracy;
	}
	public void setAccuracy(double accuracy) {
		this.accuracy = accuracy;
	}
	
	
	public String getDetectorID() {
		return detectorID;
	}
	public void setDetectorID(String detectorID) {
		this.detectorID = detectorID;
	}
	
	public double getDistance() {
		return distance;
	}
	public void setDistance(double distance) {
		this.distance = distance;
	}
	
	
	public double getDetectorPosX() {
		return detectorPosX;
	}
	public void setDetectorPosX(double detectorPosX) {
		this.detectorPosX = detectorPosX;
	}
	
	public double getDetectorPosY() {
		return detectorPosY;
	}
	public void setDetectorPosY(double detectorPosY) {
		this.detectorPosY = detectorPosY;
	}
	
	public double getDetectorPosZ() {
		return detectorPosZ;
	}
	public void setDetectorPosZ(double detectorPosZ) {
		this.detectorPosZ = detectorPosZ;
	}
}
