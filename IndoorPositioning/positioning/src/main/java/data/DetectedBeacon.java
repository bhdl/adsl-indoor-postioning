package data;

import java.util.ArrayList;

public class DetectedBeacon {
	private String id;
	private long detectionTimestamp;
	private ArrayList<ReceivedSignal> receivingDetectors = new ArrayList<ReceivedSignal>();
	
	
	public String toString() {
		String beaconString = "ID: " + id + "  TIME: " + detectionTimestamp + "   Receiving detectors: " + "\n";
		for(ReceivedSignal signal : receivingDetectors) beaconString += signal.getDetectorID() + "  " + signal.getDistance() + "\n";
		return beaconString;
	}
	
	
	public String getID() {
		return id;
	}
	public void setID(String id) {
		this.id = id;
	}
	
	public long getDetectionTimestamp() {
		return detectionTimestamp;
	}
	public void setDetectionTimestamp(long detectionTimestamp) {
		this.detectionTimestamp = detectionTimestamp;
	}
	
	public ArrayList<ReceivedSignal> getReceivingDetectors() {
		return receivingDetectors;
	}
	public void setReceivingDetectors(ArrayList<ReceivedSignal> receivingDetectors) {
		this.receivingDetectors = receivingDetectors;
	}
}
