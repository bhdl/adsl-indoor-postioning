package data;

public class GPScoordinate {
	private double latitude;
	private double longitude;
	private double altitude;
	
	
	public GPScoordinate() {}
	
	public GPScoordinate(double latitude, double longitude, double altitude) {
		this.latitude = latitude;
		this.longitude = longitude;
		this.altitude = altitude;
	}
	
	
	
	public GPScoordinate convertRelativeCoordinateToGPS(GPScoordinate origo, float relativeCoordinateSystemRoatationAngle, Position relativePosition) {
		float distance = calculateFlatDistanceOfRelativePositionFromOrigo(relativePosition);
		float bearing = getBearingAngleOfRelativeCoordinates(relativePosition.getX(), relativePosition.getY());
		bearing = corrigateBearingToEarthReferenceFrame(bearing, -relativeCoordinateSystemRoatationAngle);
		return addDistanceToCoordinate(distance, bearing);
	}
	
	
	public GPScoordinate addDistanceToCoordinate(float distance, float bearing) {
		//φ is latitude, λ is longitude, θ is the bearing (clockwise from north), δ is the angular distance d/R; d being the distance travelled, R the earth radius
	 	//φ2 = asin(sin φ1 ⋅ cos δ + cos φ1 ⋅ sin δ ⋅ cos θ)
	 	//λ2 = λ1 + atan2(sin θ ⋅ sin δ ⋅ cos φ1, cos δ − sin φ1 ⋅ sin φ2)
	 	final double angle = Math.toRadians(bearing);
	 	final double phi = distance / 6371000.0;
	 	final double radianLat = Math.toRadians(this.latitude);
		final double addedRadianLat = Math.asin(Math.sin(radianLat)*Math.cos(phi) + Math.cos(radianLat)*Math.sin(phi)*Math.cos(angle));
		final double addedRadianLon = Math.toRadians(this.longitude) + Math.atan2(Math.sin(angle)*Math.sin(phi)*Math.cos(radianLat), Math.cos(phi)-(Math.sin(radianLat)*Math.sin(addedRadianLat)));
		return new GPScoordinate(Math.toDegrees(addedRadianLat), Math.toDegrees(addedRadianLon), altitude);
	}
	
	//Bearing is measured from north and goes clockwise.
	public float getBearingAngleOfRelativeCoordinates(double x, double y) {
		float bearing = (float) Math.toDegrees(Math.atan2(x, y));
		if(bearing < 0) bearing += 360;
		return bearing;
	}
	
	public float corrigateBearingToEarthReferenceFrame(float bearing, float correctionConstantAngle) {
		float corrigatedBearing = bearing + correctionConstantAngle;
		if(corrigatedBearing > 360) corrigatedBearing -= 360;
		if(corrigatedBearing < 0) corrigatedBearing += 360;
		return corrigatedBearing;
	}
	
	public float calculateFlatDistanceOfRelativePositionFromOrigo(Position relativePosition) {
		return (float) Math.sqrt(relativePosition.getX()*relativePosition.getX() + relativePosition.getY()*relativePosition.getY());
	}
	
	
	public String toString() {
		return "Lat: " + latitude + "  Lon: " + longitude + "  Alt: " + altitude; 
	}
	
	
	public double getLatitude() {
		return latitude;
	}
	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}
	
	public double getLongitude() {
		return longitude;
	}
	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}
	
	public double getAltitude() {
		return altitude;
	}
	public void setAltitude(double altitude) {
		this.altitude = altitude;
	}
}
