package data;

public class Bounds {
	public double maxX = 0;
	public double minX = 0;
	
	public double maxY = 0;
	public double minY = 0;
	
	public double maxZ = 0;
	public double minZ = 0;
	
	public double diameter = 0;
}
