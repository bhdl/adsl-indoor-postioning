package data;

public class ReceivedSignal {
	private String beaconID = "";
	private String detectorID = "";
	private double distance;
	private long timestamp;
	private double accuracy = 1;
	
	
	public long getTimestamp() {
		return timestamp;
	}
	public void setTimestamp(long timestamp) {
		this.timestamp = timestamp;
	}
	
	public String getBeaconID() {
		return beaconID;
	}
	public void setBeaconID(String beaconID) {
		this.beaconID = beaconID;
	}
	
	public String getDetectorID() {
		return detectorID;
	}
	public void setDetectorID(String detectorID) {
		this.detectorID = detectorID;
	}
	
	public double getDistance() {
		return distance;
	}
	public void setDistance(double distance) {
		this.distance = distance;
	}
	
	public double getAccuracy() {
		return accuracy;
	}
	public void setAccuracy(double accuracy) {
		this.accuracy = accuracy;
	}
	
	public ReceivedSignal copy() {
		ReceivedSignal receivedSignal = new ReceivedSignal();
		receivedSignal.beaconID = this.beaconID;
		receivedSignal.detectorID = this.detectorID;
		receivedSignal.distance = this.distance;
		receivedSignal.timestamp = this.timestamp;
		receivedSignal.accuracy = this.accuracy;
		return receivedSignal;
	}
}
